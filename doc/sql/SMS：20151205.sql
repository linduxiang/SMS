use sms;
drop table if exists course;

drop table if exists person;

drop table if exists person_course;

drop table if exists signInTable;

drop table if exists typetable;

/*==============================================================*/
/* Table: course                                                */
/*==============================================================*/
create table course
(
   id                   int not null,
   course_name          varchar(255),
   classroom            int,
   start_time           date,
   end_time             date,
   valid                tinyint,
   primary key (id)
);

alter table course comment '课程';

/*==============================================================*/
/* Table: person                                                */
/*==============================================================*/
create table person
(
   id                   int not null,
   name                 varchar(255),
   valid                tinyint,
   password             varchar(255),
   phone                varchar(255),
   no_id                varchar(255),
   major                int,
   is_teacher           tinyint,
   primary key (id)
);

alter table person comment '用户，根据权限判断老师还是学生';

/*==============================================================*/
/* Table: person_course                                         */
/*==============================================================*/
create table person_course
(
   id                   int,
   per_id               int,
   cou_id               int,
   valid                tinyint,
   primary key (id)
);

/*==============================================================*/
/* Table: signInTable                                           */
/*==============================================================*/
create table signInTable
(
   id                   int not null,
   stu_id               int,
   c_p_id               int,
   sign_in_time         date,
   valid                tinyint,
   primary key (id)
);

alter table signInTable comment '签到表';

/*==============================================================*/
/* Table: typetable                                             */
/*==============================================================*/
create table typetable
(
   id                   int,
   text                 varchar(255),
   type_name            varchar(255),
   primary key (id)
);

alter table typetable comment '类别表';

alter table course add constraint FK_Reference_3 foreign key (classroom)
      references typetable (id) on delete restrict on update restrict;

alter table person add constraint FK_Reference_2 foreign key (major)
      references typetable (id) on delete restrict on update restrict;

alter table person_course add constraint FK_Reference_10 foreign key (per_id)
      references person (id) on delete restrict on update restrict;

alter table person_course add constraint FK_Reference_11 foreign key (cou_id)
      references course (id) on delete restrict on update restrict;

alter table person_course add constraint FK_Reference_9 foreign key (id)
      references course (id) on delete restrict on update restrict;

alter table signInTable add constraint FK_Reference_14 foreign key (c_p_id)
      references person_course (id) on delete restrict on update restrict;

alter table signInTable add constraint FK_Reference_7 foreign key (stu_id)
      references person (id) on delete restrict on update restrict;

alter table typetable add constraint FK_Reference_1 foreign key (id)
      references person (id) on delete restrict on update restrict;
