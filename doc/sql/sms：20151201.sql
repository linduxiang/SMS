
/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2015/12/1 21:20:51                           */
/*==============================================================*/

create database mysms character set utf8;
use mysms;

drop table if exists courcePerson;

drop table if exists course;

drop table if exists person;

drop table if exists signInTable;

drop table if exists typetable;

/*==============================================================*/
/* Table: courcePerson                                          */
/*==============================================================*/
create table courcePerson
(
   id                   int not null,
   per_id               int,
   cid                  int,
   isLeader             tinyint,
   valid                tinyint,
   primary key (id)
);


/*==============================================================*/
/* Table: course                                                */
/*==============================================================*/
create table course
(
   id                   int not null,
   course_name          varchar(255),
   classroom            int,
   start_time           date,
   end_time             date,
   valid                tinyint,
   primary key (id)
);


/*==============================================================*/
/* Table: person                                                */
/*==============================================================*/
create table person
(
   id                   int not null,
   name                 varchar(255),
   valid                tinyint,
   password             varchar(255),
   phone                varchar(255),
   no_id                varchar(255),
   major                int,
   is_teacher           tinyint,
   primary key (id)
);


/*==============================================================*/
/* Table: signInTable                                           */
/*==============================================================*/
create table signInTable
(
   id                   int not null,
   stu_id               int,
   c_p_id               int,
   sign_in_time         date,
   valid                tinyint,
   primary key (id)
);


/*==============================================================*/
/* Table: typetable                                             */
/*==============================================================*/
create table typetable
(
   id                   int,
   text                 varchar(255),
   type_name            varchar(255),
   primary key (id)
);


alter table courcePerson add constraint FK_Reference_4 foreign key (cid)
      references course (id) on delete restrict on update restrict;

alter table courcePerson add constraint FK_Reference_5 foreign key (per_id)
      references person (id) on delete restrict on update restrict;

alter table courcePerson add constraint FK_Reference_6 foreign key (id)
      references signInTable (id) on delete restrict on update restrict;

alter table course add constraint FK_Reference_3 foreign key (classroom)
      references typetable (id) on delete restrict on update restrict;

alter table person add constraint FK_Reference_2 foreign key (major)
      references typetable (id) on delete restrict on update restrict;

alter table signInTable add constraint FK_Reference_7 foreign key (stu_id)
      references person (id) on delete restrict on update restrict;

alter table signInTable add constraint FK_Reference_8 foreign key (c_p_id)
      references courcePerson (id) on delete restrict on update restrict;

alter table typetable add constraint FK_Reference_1 foreign key (id)
      references person (id) on delete restrict on update restrict;

