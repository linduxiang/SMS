package com.DuxiangLin.sms.controller;

import com.DuxiangLin.sms.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.annotation.Resource;

/**
 * 教师的控制层
 * User: DuxiangLin(linduxiang2@163.com)
 * Date: 2015/12/7
 * Time: 10:17
 */
@Controller
public class TeacherController {
    @Autowired
    private TeacherService teacherService;

    public TeacherService getTeacherService() {
        return teacherService;
    }

    public void setTeacherService(TeacherService teacherService) {
        this.teacherService = teacherService;
    }
}
