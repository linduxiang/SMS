package com.DuxiangLin.sms.service;

import com.DuxiangLin.sms.dao.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 教师的服务类
 * User: DuxiangLin(linduxiang2@163.com)
 * Date: 2015/12/7
 * Time: 10:17
 */
@Service("teacherService")
public class TeacherService {
    @Autowired
    private CourseDAO courseDAO;
    @Autowired
    private PersonDAO personDAO;
    @Autowired
    private SignInTableDAO signInTableDAO;
    @Autowired
    private TypeTableDAO typeTableDAO;
    @Autowired
    private PersonCourseTableDAO personCourseTableDAO;

    public PersonCourseTableDAO getPersonCourseTableDAO() {
        return personCourseTableDAO;
    }

    public void setPersonCourseTableDAO(PersonCourseTableDAO personCourseTableDAO) {
        this.personCourseTableDAO = personCourseTableDAO;
    }
    public CourseDAO getCourseDAO() {
        return courseDAO;
    }

    public void setCourseDAO(CourseDAO courseDAO) {
        this.courseDAO = courseDAO;
    }

    public PersonDAO getPersonDAO() {
        return personDAO;
    }

    public void setPersonDAO(PersonDAO personDAO) {
        this.personDAO = personDAO;
    }

    public SignInTableDAO getSignInTableDAO() {
        return signInTableDAO;
    }

    public void setSignInTableDAO(SignInTableDAO signInTableDAO) {
        this.signInTableDAO = signInTableDAO;
    }

    public TypeTableDAO getTypeTableDAO() {
        return typeTableDAO;
    }

    public void setTypeTableDAO(TypeTableDAO typeTableDAO) {
        this.typeTableDAO = typeTableDAO;
    }
}
