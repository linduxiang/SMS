package com.DuxiangLin.sms.service;

import com.DuxiangLin.sms.dao.*;
import com.DuxiangLin.sms.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 学生的服务类
 * User: DuxiangLin(linduxiang2@163.com)
 * Date: 2015/12/7
 * Time: 10:17
 */
@Service("studentService")
public class StudentService extends BaseService{
    @Autowired
    private CourseDAO courseDAO;
    @Autowired
    private PersonDAO personDAO;
    @Autowired
    private SignInTableDAO signInTableDAO;
    @Autowired
    private TypeTableDAO typeTableDAO;
    @Autowired
    private PersonCourseTableDAO personCourseTableDAO;

    public PersonCourseTableDAO getPersonCourseTableDAO() {
        return personCourseTableDAO;
    }

    public void setPersonCourseTableDAO(PersonCourseTableDAO personCourseTableDAO) {
        this.personCourseTableDAO = personCourseTableDAO;
    }

    public CourseDAO getCourseDAO() {
        return courseDAO;
    }

    public void setCourseDAO(CourseDAO courseDAO) {
        this.courseDAO = courseDAO;
    }

    public PersonDAO getPersonDAO() {
        return personDAO;
    }

    public void setPersonDAO(PersonDAO personDAO) {
        this.personDAO = personDAO;
    }

    public SignInTableDAO getSignInTableDAO() {
        return signInTableDAO;
    }

    public void setSignInTableDAO(SignInTableDAO signInTableDAO) {
        this.signInTableDAO = signInTableDAO;
    }

    public TypeTableDAO getTypeTableDAO() {
        return typeTableDAO;
    }

    public void setTypeTableDAO(TypeTableDAO typeTableDAO) {
        this.typeTableDAO = typeTableDAO;
    }

    /**
     * 根据typeName得到TypeTable数组
     * @param typeName
     * @return
     */
    public List<TypeTable> listByText(String typeName){
        return this.typeTableDAO.listByText(typeName);
    }

    /**
     * 删除注册表
     * @param signInTable
     */
    public void deleteSignInTable(SignInTable signInTable){
        signInTableDAO.delete(signInTable);
    }

    /**
     * 删除人物
     * @param person
     */
    public void delPerson(Person person){
        personDAO.delete(person);
    }

    /**
     * 删除课程
     * @param course
     */
    public void delCourse(Course course){
        courseDAO.delete(course);
    }

    /**
     * 删除类型
     * @param typeTable
     */
    public void delTypeTable(TypeTable typeTable){
        typeTableDAO.delete(typeTable);
    }

    /**
     * 删除人物-课程
     * @param personCourseTable
     */
    public void delPCTable(PersonCourseTable personCourseTable){
        personCourseTableDAO.delete(personCourseTable);
    }
}
