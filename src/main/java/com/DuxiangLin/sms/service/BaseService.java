package com.DuxiangLin.sms.service;

import com.DuxiangLin.sms.dao.BaseDAO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;


/**
 * 服务类基类
 * User: DuxiangLin(linduxiang2@163.com)
 * Date: 2015/12/7
 * Time: 18:30
 */
public class BaseService<T> {
    @Autowired
    public BaseDAO baseDAO;
    protected Logger logger = Logger.getLogger(this.getClass());

    /**
     * 添加
     * @param t
     */
    public void add(T t){
        logger.info("********添加" + t.getClass().getSimpleName() + "********");
        baseDAO.add(t);
    }
    /**
     * 更新
     * @param t
     */
    public void update(T t){
        logger.info("********更新" + t.getClass().getSimpleName() + "********");
        baseDAO.update(t);
    }

    /**
     * 通过id找到对应的T
     * @param t
     * @param id
     */
    public T findById(Class<T> t, int id){
        logger.info("********找到：id为" + id + "的" + t.getClass().getSimpleName() + "********");
        return (T) baseDAO.findById(t, id);
    }

    /**
     * 删除
     * todo 根据不同的对象进行删除
     * @param t
     */
    public void delete(T t){
        logger.info("********删除" + t.getClass().getSimpleName() + "********");
    }

    /**
     * 返回对应T的数组
     * @param t
     * @return
     */
    public List<T> findAll(Class<T> t){
        logger.info("********得到全部"+t.getSimpleName()+"********");
        return baseDAO.findAll(t);
    }

}
