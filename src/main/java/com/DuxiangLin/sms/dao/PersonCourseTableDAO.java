package com.DuxiangLin.sms.dao;

import com.DuxiangLin.sms.model.PersonCourseTable;
import org.springframework.stereotype.Repository;

/**
 * User: DuxiangLin(linduxiang2@163.com)
 * Date: 2015/12/7
 * Time: 13:43
 */
@Repository
public class PersonCourseTableDAO extends BaseDAO<PersonCourseTable>{
    @Override
    public void delete(PersonCourseTable personCourseTable) {
        super.delete(personCourseTable);
        personCourseTable.setValid(false);
        getSession().update(personCourseTable);
    }
}
