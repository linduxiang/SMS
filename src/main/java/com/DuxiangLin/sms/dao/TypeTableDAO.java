package com.DuxiangLin.sms.dao;

import com.DuxiangLin.sms.model.TypeTable;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * TypeTable的数据库操作
 * User: DuxiangLin(linduxiang2@163.com)
 * Date: 2015/12/7
 * Time: 10:16
 */
@Repository
public class TypeTableDAO extends BaseDAO<TypeTable>{

    /**
     * 删除一个TypeTable对象
     * TODO 将valid设为false
     * @param typeTable
     */
    public void delete(TypeTable typeTable){
        typeTable.setValid(false);
        getSession().update(typeTable);
    }

    /**
     * 根据typeName得到TypeTable数组
     * @param typeName
     * @return
     */
    public List<TypeTable> listByText(String typeName){
        String sql = "select t from TypeTable as t " +
                "where t.typeName = :typeName";
        return this.getSession().createQuery(sql).setParameter("typeName",typeName).list();
    }
}
