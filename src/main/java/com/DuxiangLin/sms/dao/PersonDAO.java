package com.DuxiangLin.sms.dao;

import com.DuxiangLin.sms.model.Person;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Person的数据库操作
 * User: DuxiangLin(linduxiang2@163.com)
 * Date: 2015/12/7
 * Time: 10:16
 */
@Repository
public class PersonDAO extends BaseDAO<Person>{
    @Override
    public void delete(Person person) {
        System.out.println("删除成功");
        super.delete(person);
        person.setValid(false);
        getSession().update(person);
    }

    /**
     * 通过学号得到学生
     * @param noId
     * @return
     */
    public Person findByNoId(String noId){
        String sql = "select t from Person t " +
                "where t.noId = :noId";
        return (Person) getSession().createQuery(sql).setParameter("noId",noId).uniqueResult();
    }

    /**
     * 根据姓名模糊查找
     * @param name
     * @return
     */
    public List<Person> findByName(String name){
        String sql = "select p from Person p " +
                "where p.name like :name ";
        return getSession().createQuery(sql).setParameter("name", "%" + name + "%").list();
    }

}
