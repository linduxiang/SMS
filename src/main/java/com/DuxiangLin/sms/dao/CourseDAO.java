package com.DuxiangLin.sms.dao;

import com.DuxiangLin.sms.model.Course;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


/**
 * Course的数据库操作
 * User: DuxiangLin(linduxiang2@163.com)
 * Date: 2015/12/7
 * Time: 10:16
 */
@Repository
public class CourseDAO extends BaseDAO<Course>{
    @Override
    public void delete(Course course) {
        super.delete(course);
        course.setValid(false);
        getSession().update(course);
    }


}
