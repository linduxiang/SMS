package com.DuxiangLin.sms.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 数据库操作的基类
 * User: DuxiangLin(linduxiang2@163.com)
 * Date: 2015/12/7
 * Time: 17:55
 */
@Repository
public class BaseDAO<T> {
    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public Session getSession(){
        return this.getSessionFactory().getCurrentSession();
    }

    /**
     * 添加
     * @param t
     */
    public void add(T t){
        getSession().save(t);
    }

    /**
     * 更新
     * @param t
     */
    public void update(T t){
        getSession().update(t);
    }

    /**
     * 通过id找到对应的T
     * @param t
     * @param id
     */
    public T findById(Class<T> t,int id){
        return getSession().get(t,id);
    }

    /**
     * 删除
     * todo 根据不同的对象进行删除
     * @param t
     */
    public void delete(T t){
    }

    /**
     * 返回对应T的数组
     * @param t
     * @return
     */
    public List<T> findAll(Class<T> t){
        return getSession().createQuery("select en from "+t.getSimpleName() + " en").list();
    }
}
