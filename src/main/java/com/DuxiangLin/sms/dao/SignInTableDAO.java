package com.DuxiangLin.sms.dao;

import com.DuxiangLin.sms.model.SignInTable;
import org.springframework.stereotype.Repository;

/**
 * SignInTable的数据库操作
 * User: DuxiangLin(linduxiang2@163.com)
 * Date: 2015/12/7
 * Time: 10:16
 */
@Repository
public class SignInTableDAO extends BaseDAO<SignInTable>{
    @Override
    public void delete(SignInTable signInTable) {
        super.delete(signInTable);
        signInTable.setValid(false);
        getSession().update(signInTable);
    }
}
