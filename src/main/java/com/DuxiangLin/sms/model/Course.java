package com.DuxiangLin.sms.model;

import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.*;
/**
 * 课程类
 * User: DuxiangLin(linduxiang2@163.com)
 * Date: 2015/12/4
 * Time: 16:25
 */
@Entity
@Table(name = "course")
public class Course {
    private int id;
    private String courseName;
    private TypeTable classroom;
    private Date startTime;
    private Date endTime;
    private boolean valid;
    private List<Person> personList;
    private TypeTable section;
    private int week;

    @OneToOne(targetEntity = TypeTable.class)
    @JoinColumn(name = "section")
    public TypeTable getSection() {
        return section;
    }

    public void setSection(TypeTable section) {
        this.section = section;
    }

    @Column(name = "week")
    public int getWeek() {
        return week;
    }

    public void setWeek(int week) {
        this.week = week;
    }

    @OneToMany(targetEntity = Person.class)
    @JoinTable(name = "person_course",
            joinColumns = {@JoinColumn(name = "cou_id")},
            inverseJoinColumns = {@JoinColumn(name = "per_id")})
    public List<Person> getPersonList() {
        return personList;
    }

    public void setPersonList(List<Person> personList) {
        this.personList = personList;
    }

    @OneToOne(targetEntity = TypeTable.class)
    @JoinColumn(name = "classroom")//映射外键列
    @Cascade(CascadeType.ALL)
    public TypeTable getClassroom() {
        return classroom;
    }

    public void setClassroom(TypeTable classroom) {
        this.classroom = classroom;
    }

    @Column(name = "course_name")
    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    @Column(name = "end_time")
    @Temporal(TemporalType.DATE)//指定该属性映射到数据库的date
    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    @Id
    @GeneratedValue
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "start_time")
    @Temporal(TemporalType.DATE)
    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    @Column
    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", courseName='" + courseName + '\'' +
                ", classroom=" + classroom +
                ", valid=" + valid +
                '}';
    }
}
