package com.DuxiangLin.sms.model;

import javax.persistence.*;
import java.util.*;

/**
 * 签到表
 * User: DuxiangLin(linduxiang2@163.com)
 * Date: 2015/12/5
 * Time: 22:08
 */
@Entity
@Table(name = "sign_in_table2")
public class SignInTable {
    private int id;
    private Date signInTime;
    private boolean valid;
    private int personCourseTable;

    @Id
    @GeneratedValue
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "sign_in_time")
    @Temporal(TemporalType.DATE)
    public Date getSignInTime() {
        return signInTime;
    }

    public void setSignInTime(Date signInTime) {
        this.signInTime = signInTime;
    }

    @Column(name = "valid")
    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    @Column(name = "c_p_id")
    public int getPersonCourseTable() {
        return personCourseTable;
    }

    public void setPersonCourseTable(int personCourseTable) {
        this.personCourseTable = personCourseTable;
    }

    @Override
    public String toString() {
        return "SignInTable{" +
                "id=" + id +
                ", signInTime=" + signInTime +
                ", valid=" + valid +
                ", personCourseTable=" + personCourseTable +
                '}';
    }
}
