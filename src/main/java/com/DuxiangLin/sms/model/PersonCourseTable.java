package com.DuxiangLin.sms.model;

import javax.persistence.*;

/**
 * User: DuxiangLin(linduxiang2@163.com)
 * Date: 2015/12/7
 * Time: 13:36
 */
@Entity
@Table(name = "person_course")
public class PersonCourseTable {
    private int id;
    private boolean valid;
    private int person;
    private int course;

    @Id
    @GeneratedValue
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "cou_id")
    public int getCourse() {
        return course;
    }

    public void setCourse(int course) {
        this.course = course;
    }

    @Column(name = "per_id")
    public int getPerson() {
        return person;
    }

    public void setPerson(int person) {
        this.person = person;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    @Override
    public String toString() {
        return "PersonCourseTable{" +
                "course=" + course +
                ", id=" + id +
                ", valid=" + valid +
                ", person=" + person +
                '}';
    }
}