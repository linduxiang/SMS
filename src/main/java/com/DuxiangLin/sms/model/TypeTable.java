package com.DuxiangLin.sms.model;

import javax.persistence.*;

/**
 * 类型表
 * User: DuxiangLin(linduxiang2@163.com)
 * Date: 2015/12/4
 * Time: 16:25
 */
@Entity
@Table(name = "typetable")
public class TypeTable {
    private int id;
    private String text;
    private String typeName;
    private boolean valid;

    @Id
    @GeneratedValue
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "text")
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Column(name = "type_name")
    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    @Column(name = "valid")
    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    @Override
    public String toString() {
        return "TypeTable{" +
                "id=" + id +
                ", text='" + text + '\'' +
                ", typeName='" + typeName + '\'' +
                ", valid=" + valid +
                '}';
    }
}
