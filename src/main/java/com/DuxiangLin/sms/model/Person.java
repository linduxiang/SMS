package com.DuxiangLin.sms.model;

import javax.persistence.*;
import java.util.Set;

/**
 * 用户类
 * User: DuxiangLin(linduxiang2@163.com)
 * Date: 2015/12/4
 * Time: 16:22
 */
@Entity
@Table(name = "person")
public class Person {
    private int id;
    private String name;
    private boolean valid;
    private String password;
    private String phone;
    private String noId;
    private TypeTable major;
    private boolean isTeacher;

    private Set<Course> courseList;

    @Id
    @GeneratedValue
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "is_teacher")
    public boolean isTeacher() {
        return isTeacher;
    }

    public void setTeacher(boolean isTeacher) {
        this.isTeacher = isTeacher;
    }

    @OneToOne(targetEntity = TypeTable.class)
    @JoinColumn(name = "major")
    public TypeTable getMajor() {
        return major;
    }

    public void setMajor(TypeTable major) {
        this.major = major;
    }

    @Basic
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "no_id")
    public String getNoId() {
        return noId;
    }

    public void setNoId(String noId) {
        this.noId = noId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    @OneToMany(targetEntity = Course.class)
    @JoinTable(name = "person_course",joinColumns = {@JoinColumn(name = "per_id")},
            inverseJoinColumns = {@JoinColumn(name = "cou_id")})
    public Set<Course> getCourseList() {
        return courseList;
    }

    public void setCourseList(Set<Course> courseList) {
        this.courseList = courseList;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", noId='" + noId + '\'' +
                ", name='" + name + '\'' +
                ", major=" + major +
                ", isTeacher=" + isTeacher +
                ", password='" + password + '\'' +
                ", phone='" + phone + '\'' +
                ", valid=" + valid +
                '}';
    }
}
