package com.DuxiangLin.sms.service;

import com.DuxiangLin.sms.dao.TypeTableDAO;
import com.DuxiangLin.sms.model.Person;
import com.DuxiangLin.sms.model.TypeTable;
import com.DuxiangLin.sms.util.MD5;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

public class StudentServiceTest extends TestCase {

    private StudentService studentService;
    @Autowired
    public StudentService getStudentService() {
        return studentService;
    }

    public void setStudentService(StudentService studentService) {
        this.studentService = studentService;
    }

    @Before
    public void setUp() throws Exception {
        super.setUp();
        ApplicationContext ac = new ClassPathXmlApplicationContext(new String[]{"applicationContext.xml"});
        studentService = (StudentService) ac.getBean("studentService");
    }

    @Test
    public void testAddOneType() throws Exception {
        TypeTable table = new TypeTable();
        table.setText("软件工程");
        table.setTypeName("专业");
        table.setValid(true);
        studentService.add(table);
        table.setId(8);
        Person p = new Person();
        p.setValid(true);
        p.setMajor(table);
        p.setName("林督翔");
        p.setPassword(new MD5().getkeyBeanofStr("linduxiang"));
        p.setNoId("201330320618");
        p.setPhone("18814127590");
        p.setTeacher(false);
        studentService.add(p);
    }

    @Test
    public void testFindById(){
        System.out.println(studentService.findById(Person.class,9));
    }

    @Test
    public void testDel(){
        studentService.delete(studentService.findById(Person.class,9));
    }

    @Test
    public void testListTypes(){
        List<Person> list = studentService.findAll(TypeTable.class);
        for (int i = 0; i < list.size(); i++) {
            System.out.println("i:"+list.get(i));
        }
    }

    @Test
    public void testListByName(){
        List<TypeTable> list = studentService.listByText("教室");
        for (int i = 0; i < list.size(); i++) {
            System.out.println("i:"+list.get(i));
        }
    }
}