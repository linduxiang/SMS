package com.DuxiangLin.sms.dao;

import com.DuxiangLin.sms.model.TypeTable;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.annotation.Resource;

public class TypeTableDAOTest extends TestCase {
    @Autowired
    private TypeTableDAO dao;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        ApplicationContext ac = new ClassPathXmlApplicationContext(new String[]{"applicationContext.xml"});
        dao = (TypeTableDAO) ac.getBean("typeTableDAO");
    }

    @Test
    public void testAddOneType() throws Exception {
        TypeTable table = new TypeTable();
        table.setText("教三101");
        table.setTypeName("教室");
        table.setValid(false);
        dao.add(table);
    }

    public void testUpdateOneType() throws Exception {

    }

    public void testDeleteOneType() throws Exception {

    }

    public void testListAllType() throws Exception {

    }

    public void testFindById() throws Exception {

    }

    public void testListByText() throws Exception {

    }
}